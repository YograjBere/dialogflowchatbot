import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CandidateChatComponent } from './candidate-chat/candidate-chat.component';
import { LoginComponent } from './login/login.component';
import { RecruiterChatComponent } from './recruiter-chat/recruiter-chat.component';

const routes: Routes = [
	{ path: 'login', component: LoginComponent },
	{ path: '', component: CandidateChatComponent },
  { path: 'operator', component: RecruiterChatComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
