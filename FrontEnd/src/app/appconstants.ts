export class AppConstants {
  static CUSTOMER_MESSAGE_EVENT = "customer message";
  static OPERATOR_MESSAGE_EVENT = "operator message";
  static NEW_OPERATOR_JOINED_EVENT = "New operator connected";
  static OPERATOR_REQUESTED_MESSAGE_EVENT = "operator requested";
  static OPERATOR_JOINED_CONVERSATION_MESSAGE_EVENT = "operator joined chat with customer";
  static SENT_MESSAGE = "s";
  static RECEIVED_MESSAGE = "r";
  static CUSTOMER_CONNECTED_EVENT="customer connected";
  static CUSTOMER_DISCONNECTED_EVENT="customer disconnected";
  static SYSTEM_ERROR_EVENT="connect_error";
  static CUSTOMER_DEFAULT_NAME="Candidate";
  static OPERATOR_DEFAULT_NAME="Operator";
}
