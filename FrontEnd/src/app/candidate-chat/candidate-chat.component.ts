import { Component, OnInit } from '@angular/core';
import { io } from "socket.io-client";

import { faLocationArrow, faPaperclip } from '@fortawesome/free-solid-svg-icons';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppConstants } from '../appconstants';
import { ChatMessage } from '../models/chatmessage';

@Component({
  selector: 'app-candidate-chat',
  templateUrl: './candidate-chat.component.html',
  styleUrls: ['./candidate-chat.component.css']
})
export class CandidateChatComponent implements OnInit {

  faLocationArrow = faLocationArrow;
  faPaperclip = faPaperclip;
  title = 'chatbot';
  currentMessage = "";
  socket = io("/customer");
  messages: Array<ChatMessage> = new Array<ChatMessage>();
  isShow = true;
  changeText: boolean = false;

  ngOnInit(): void {
    this.socket.on("connect", () => {
      console.log(this.socket.id);
    });

    this.socket.on("connect_error", (e) => {
      console.log(e);
    });

    this.socket.on(AppConstants.CUSTOMER_MESSAGE_EVENT, this.onReceive.bind(this));
  }

  onReceive(msg: any) {
    var received_message = { type: AppConstants.RECEIVED_MESSAGE, msg: msg, time: new Date() };
    this.messages.push(received_message);
  }

  onSend() {
    let currMessage = this.currentMessage;
    this.socket.emit(AppConstants.CUSTOMER_MESSAGE_EVENT, currMessage);
    this.messages.push({ type: AppConstants.SENT_MESSAGE, msg: currMessage, time: new Date() });
    this.currentMessage = '';
  }

  onKey(event: any) {
    if (event.keyCode == 13) {
      this.onSend();
    }
  }

  tooltip() {
    this.changeText = true;
  }

  toggleDisplay() {
    this.isShow = !this.isShow;
  }
}
