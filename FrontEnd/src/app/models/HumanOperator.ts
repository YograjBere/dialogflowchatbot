import { Customer } from "./customer";

export class HumanOperator {

  constructor(public id: string,
    public name: string,
    public customerList: Map<string, Customer>,
    public connectedWithCusomer?: Customer) {
  }

  get isConnectedWithCustomer(): boolean {
    if (this.connectedWithCusomer)
      return true;

    return false;
  }
}
