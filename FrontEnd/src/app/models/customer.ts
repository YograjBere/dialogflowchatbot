import { ChatMessage } from "./chatmessage";
import { HumanOperator } from "./HumanOperator";

export class Customer
{
  constructor(public id: string,
    public name: string,
    public isConnected: boolean,
    public isSelected: boolean,
    public messages: Array<ChatMessage>,
    public isRequestingOperator: boolean = false,
    public connectedWithOperator?: HumanOperator) {}
}
