export class ChatMessage {
  constructor(public type: string, public msg: string, public time: Date, public isAgentResponse?: boolean, public customerId?: string) {}
}
