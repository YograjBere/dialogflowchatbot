import { Component, OnInit } from '@angular/core';
import { io } from "socket.io-client";
import { AppConstants } from '../appconstants';
import { ChatMessage } from '../models/chatmessage';
import { faLocationArrow, faPaperclip, faSearch, faHandshake, faHandsHelping } from '@fortawesome/free-solid-svg-icons';
import { Customer } from '../models/customer';
import { HumanOperator } from '../models/HumanOperator';

class operatorMessage {
  constructor(private customerId: string, private utterance: string) {

  }
}

@Component({
  selector: 'app-recruiter-chat',
  templateUrl: './recruiter-chat.component.html',
  styleUrls: ['./recruiter-chat.component.css']
})
export class RecruiterChatComponent implements OnInit {
  title = "Recruitment";
  socket = io("/operator");
  faLocationArrow = faLocationArrow;
  faPaperclip = faPaperclip;
  faSearch = faSearch;
  faHandsHelping = faHandsHelping;
  messages: Array<ChatMessage> = new Array<ChatMessage>();
  currentMessage = "";
  connectedCustomers: Map<string, Customer> = new Map<string, Customer>();
  selectedCustomer: Customer = <Customer><unknown>{ message: [] };
  customerCount: number = 0;
  operatorCount: number = 0;
  operators: Array<HumanOperator> = new Array<HumanOperator>();
  currentOperator: HumanOperator = <HumanOperator>{};

  constructor() { }

  ngOnInit(): void {
    this.addHandlers();
  }

  addHandlers() {
    this.socket.on("connect", () => {
      var tempOperator: HumanOperator = {
        id: this.socket.id,
        name: (AppConstants.OPERATOR_DEFAULT_NAME + " " + this.operators.length.toString()),
        customerList: this.connectedCustomers,
        isConnectedWithCustomer: false
      };

      this.currentOperator = tempOperator;
      this.operators.push(tempOperator);
      this.socket.emit(AppConstants.NEW_OPERATOR_JOINED_EVENT, tempOperator);
    });

    this.socket.on(AppConstants.CUSTOMER_CONNECTED_EVENT, this.onCustomerConnected.bind(this));
    this.socket.on(AppConstants.SYSTEM_ERROR_EVENT, this.onSystemError.bind(this));
    this.socket.on(AppConstants.CUSTOMER_MESSAGE_EVENT, this.onCustomerMessageReceived.bind(this));
    this.socket.on(AppConstants.CUSTOMER_DISCONNECTED_EVENT, this.onCustomerDisconnected.bind(this));
    this.socket.on(AppConstants.OPERATOR_REQUESTED_MESSAGE_EVENT, this.onOperatorRequested.bind(this));
    this.socket.on(AppConstants.OPERATOR_JOINED_CONVERSATION_MESSAGE_EVENT, this.onOperatorJoinedConversation.bind(this));
    this.socket.on(AppConstants.NEW_OPERATOR_JOINED_EVENT, this.onNewOperatorConnected.bind(this));
  }

  onNewOperatorConnected(humanOperator: HumanOperator) {
    this.operatorCount++;
    this.operators.push({
      id: humanOperator.id,
      name: humanOperator.name,
      customerList: humanOperator.customerList,
      isConnectedWithCustomer: humanOperator.isConnectedWithCustomer
    });
    console.log(`New operator connected with socket Id:${humanOperator.id}`);
  }

  joinConversationClick(customer: Customer) {
    console.log(`operator joining conversation with customer ${customer.name}`);
    customer.isRequestingOperator = false;
    this.socket.emit(AppConstants.OPERATOR_JOINED_CONVERSATION_MESSAGE_EVENT, { operatorId: this.currentOperator.id, customer: customer });
  }

  onOperatorJoinedConversation(data: any) {
    let {operatorId, customer} = data;
    console.log(`Operator ${operatorId} joined conversation with customer ${customer.name}`);
    this.currentOperator.customerList.delete(customer.id);
  }

  onSend(customerId: any) {
    let currMessage = this.currentMessage.trim();
    if (currMessage != '') {
      this.onOperatorSendMessge({ customerId: customerId, utterance: currMessage });
      this.currentMessage = '';
    }
  }

  onKey(event: any, customerId: any) {
    if (event.keyCode == 13) {
      this.onSend(customerId);
    }
  }

  onCustomerConnected(customerId: any) {
    this.customerCount++;
    var customer = new Customer(customerId, AppConstants.CUSTOMER_DEFAULT_NAME + " " + this.customerCount, true, true, []);
    this.connectedCustomers.set(customerId, customer);
    this.selectCustomer(customerId);
  }

  onCustomerDisconnected(customerId: string) {
    var connectedCustomer = this.connectedCustomers.get(customerId);
    if (connectedCustomer && connectedCustomer?.isConnected) {
      connectedCustomer.isConnected = false;
    }
  }

  onCustomerMessageReceived(msg: any) {
    console.log(msg);
    var customer = this.connectedCustomers.get(msg.customerId);
    if (customer) {
      var messageFromcusomer = customer?.name || AppConstants.CUSTOMER_DEFAULT_NAME;
      var received_message = new ChatMessage(msg.isAgentResponse ? AppConstants.SENT_MESSAGE : AppConstants.RECEIVED_MESSAGE, (msg.isAgentResponse ? "Bot:" : messageFromcusomer + ":") + msg.utterance, new Date(), msg.isAgentResponse, msg.id);
      customer?.messages.push(received_message);
    }
  }

  onSystemError(error: any) {
    var errorText;
    // If we get this custom error type, the error was due to an operator mistake; display it
    // in a friendlier manner (without the word 'Error')
    if (error.type === 'CustomerModeError') {
      errorText = error.message;
      // Otherwise, print the error type and message
    } else {
      errorText = error.type + ' - ' + error.message;
    }
    console.log(errorText);
    if (!this.selectedCustomer) return;
    // currentTab.window.append($('<li class="operator-error">').text(errorText));
  }

  onOperatorSendMessge(msg: any) {
    var customer = this.connectedCustomers.get(msg.customerId);
    if (!customer) {
      console.log('Received operator message to unknown customer id: ' + JSON.stringify(msg));
      return;
    }

    this.socket.emit(AppConstants.OPERATOR_MESSAGE_EVENT, { customerId: msg.customerId, utterance: msg.utterance, isAgentResponse: false });
    customer.messages.push(new ChatMessage(AppConstants.SENT_MESSAGE, msg.utterance, new Date(), false, msg.customerId));

  }

  onNotifyOperatorRequest(customerId: any) {
    var customer = this.connectedCustomers.get(customerId);
    if (!customer) {
      console.log('Received operator request from unknown customer id: ' + customerId);
      return;
    }

    this.selectedCustomer = customer;
    // start displaying join button, once requested a human operator
  };

  onCustomerTabClicked(customerId: any) {
    this.selectCustomer(customerId);
  }

  selectCustomer(customerId: any) {
    var prevSelectedCustomerId = this.selectedCustomer?.id;
    var customerToSelect = this.connectedCustomers.get(customerId);
    if (customerToSelect) {
      customerToSelect.isSelected = true;
      this.selectedCustomer = customerToSelect;
      var prevCustomer = this.connectedCustomers.get(prevSelectedCustomerId);
      if (prevCustomer)
        prevCustomer.isSelected = false;
    }
  }

  onOperatorRequested(customerId: any) {
    if (!this.connectedCustomers.get(customerId)) {
      console.log('Received operator request from unknown customer id: ' + customerId);
      return;
    }

    var customerRequestingOperator = this.connectedCustomers.get(customerId);
    if (customerRequestingOperator)
      customerRequestingOperator.isRequestingOperator = true;

    // setCurrentTab(connectedCustomers[customerId]);
    console.log(`Operator requested! for customer ${customerId}`);
  }
}
