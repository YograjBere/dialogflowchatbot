import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { CandidateChatComponent } from './candidate-chat/candidate-chat.component';
import { RecruiterChatComponent } from './recruiter-chat/recruiter-chat.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CandidateChatComponent,
    RecruiterChatComponent
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
