$currDir = Split-Path $PSCommandPath
$service_account_path = "DF_SERVICE_ACCOUNT_PATH";
$project_id = "DF_PROJECT_ID";
$project_id_json_key = "project_id";

function perform-operation($confirmation)
{
    $selected_agent = $chatbot_choices[$confirmation];
    $bot_security_acc_info = gc $selected_agent | Out-String | ConvertFrom-Json;    
    set-environment-variable $project_id ($bot_security_acc_info.$($project_id_json_key))
    set-environment-variable $service_account_path (join-path $currDir $chatbot_choices[$confirmation])
}

function set-environment-variable($key, $value)
{    
    [Environment]::SetEnvironmentVariable($key, $value, "User");
    Write-Host "set environment variable $key=$value";
}

push-location $currDir

try {
        $confirmation = [int] (Read-Host 'Which chatbot agent do you want set (1/2)?');
        $chatbot_choices = @('candidate-chatbot-nhyt-8c9dbb405d15.json', 'chatbotagent-ywbx-75ee71aff620.json');
        if($confirmation -lt 1 -or $confirmation -gt 2)
        {
            throw 'Invalid input choice'
        }

        perform-operation($confirmation - 1);
}
catch {
    Write-Warning $Error[0];
}

