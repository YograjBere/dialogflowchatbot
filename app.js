// Load third party dependencies
const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http, {
    cors: {
        origin: "*",
        methods: ["GET", "POST"],
        credentials: false
    }
});

const port = process.env.PORT || 3000;

// Load our custom classes
const CustomerStore = require('./customerStore.js');
const MessageRouter = require('./messageRouter.js');

// Grab the service account credentials path from an environment variable
const keyPath = process.env.DF_SERVICE_ACCOUNT_PATH;
if (!keyPath) {
    console.log('You need to specify a path to a service account keypair in environment variable DF_SERVICE_ACCOUNT_PATH. See README.md for details.');
    process.exit(1);
}

// Load and instantiate the Dialogflow client library
const { SessionsClient } = require('dialogflow');
const dialogflowClient = new SessionsClient({
    keyFilename: keyPath
})

// Grab the Dialogflow project ID from an environment variable
const projectId = process.env.DF_PROJECT_ID;
if (!projectId) {
    console.log('You need to specify a project ID in the environment variable DF_PROJECT_ID. See README.md for details.');
    process.exit(1);
}

// Instantiate our app
const customerStore = new CustomerStore();
const messageRouter = new MessageRouter({
    customerStore: customerStore,
    dialogflowClient: dialogflowClient,
    projectId: projectId,
    customerRoom: io.of('/customer'),
    operatorRoom: io.of('/operator')
});

// Serve static html files for the customer and operator clients
app.use(express.static('static/dist/chatbot-ui-app'));


app.get('/careers', (req, res) => {
    res.sendFile(`${__dirname}/static/Carrers_page.html`);
});

app.get('/', (req, res) => {
    res.sendFile(`${__dirname}/static/dist/chatbot-ui-app/index.html`);
});

// Begin responding to websocket and http requests
messageRouter.handleConnections();
http.listen(port, () => {
    console.log(`Listening on *:${port}`);
});