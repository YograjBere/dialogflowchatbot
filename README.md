# Candidate chatbot MVP
## How to get started:

Please follow below steps:

- Install npm packages in both Frontend and BackEnd folders: **npm i**
- Add environment variable **"DF_SERVICE_ACCOUNT_PATH"**=Path to service account json file path and **"DF_PROJECT_ID"**=Project Id
- Edit "Backend/start_server.bat" to set environment variable.
- Start **"npm run start from FrontEnd folder"** and **"Backend/stat_server.bat"**
- Open **"http://localhost:3000/careers"** to view the result
